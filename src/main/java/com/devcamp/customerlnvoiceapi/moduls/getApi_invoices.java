package com.devcamp.customerlnvoiceapi.moduls;
import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class getApi_invoices {
    @GetMapping("/invoices")
    public ArrayList<Invoice> splitString() {
       // khởi tạo 3 đối tượng khách hàng 
       Customer  khach_hang1 = new Customer(10226, "thuong", 20);
       Customer  khach_hang2 = new Customer(10227, "duong", 50);
       Customer  khach_hang3 = new Customer(10228, "linh", 70);

       System.out.println(khach_hang1);
       System.out.println(khach_hang2);
       System.out.println(khach_hang3);

       // Khởi tạo 3 đối tượng hóa đơn
       Invoice invoice1 = new Invoice(1, khach_hang1, 120000);
       Invoice invoice2 = new Invoice(2, khach_hang2, 120000);
       Invoice invoice3 = new Invoice(3, khach_hang3, 120000);
       
       // thêm các hóa đơn vào ARR list 
        ArrayList<Invoice> listInvoice = new ArrayList<Invoice>();
        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);
          
        return listInvoice;
    } 
    
}
